'''
Created on Aug 28, 2016

@author: gary
'''

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches

# initial plot_matrix code from: 
#   http://stackoverflow.com/questions/31998226/how-to-change-color-bar-to-align-with-main-plot-in-matplotlib

#note that we are forcing width:height=1:1 here, 
#as 0.9*8 : 0.9*8 = 1:1, the figure size is (8,8)
#if the figure size changes, the width:height ratio here also need to be changed
def update_matrix(mat, im, ax, title='example'):
    im.set_array(mat)
    ax.set_title(title)
    plt.pause(.5)

def plot_matrix(mat, im, ax, goal, sink, figsize, title='example'):
    for (x,y) in goal:
        rect = patches.Rectangle((y-.500,x-.500),1,1,edgecolor='lime',facecolor='lime')
        ax.add_patch(rect)
    for (x,y) in sink:
        rect = patches.Rectangle((y-.500,x-.500),1,1,edgecolor='r'   ,facecolor='r'   )
        ax.add_patch(rect)

    ax.grid(False)
    cax = plt.axes([0.85, 0.05, 0.05,0.9 ])
    plt.colorbar(mappable=im, cax=cax)
    update_matrix(mat, im, ax, title)
    return ax

def main():
    #data = np.random.random((20, 20))
    plt.show()
    ax = plt.axes([0, 0.05, 0.9, 0.9 ]) #left, bottom, width, height
    pCorrectMove = .40; # otherwise completely scatters with the remaining probability (with correct move still a possibility)
    data = np.zeros((20, 20))
    goal = [(17,17)]
    sink = [(12,18),(14,18),(13,18),(13,17),(13,16),(13,15),(13,14),(9,10),(10,10),(11,10),(12,10),(13,10),(14,10),(15,10),(7,11),(7,11),(7,13),(7,14),(7,15),(3,10),(4,9),(5,8),(6,7),(7,6),(8,5),(9,4),(10,3),(11,2),(12,1)]
    for (x,y) in goal:
        data[x,y]=1
    im = ax.imshow(data, interpolation='nearest', cmap=plt.get_cmap('Blues'))
        
    ax = plot_matrix(data, im, ax, goal, sink, (10, 10)) 
    
    for i in range(200):
        data0 = np.copy(data)
        for     x in range(20):
            for y in range(20):
                #Fill this in ???????????????????????????????????
                placeholder = 0
        for (x,y) in goal:
            data[x,y]=1 # This is the probability of a win if you are at (x,y) that is part of "goal"
        for (x,y) in sink:
            data[x,y]=0 # This is the probability of a win if you are at (x,y) that is part of "sink"
        update_matrix(data, im, ax,"iteration ("+str(i+1)+")")
        #ax, cax = plot_matrix(data, goal, sink, (10, 10), "iteration ("+str(i+1)+")") 
    ax = plot_matrix(data, im, ax, goal, sink, (10, 10), "iteration (FINAL)") 
    plt.pause(600)

main()